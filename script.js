$('.tabs__caption li').click(function () {
    $(this).addClass('active').siblings().removeClass('active');
    const tabContentId = $(this).data("tabs");
    $(`#${tabContentId}`).parent().children().removeClass('active');
    $(`#${tabContentId}`).addClass('active');
});